import numpy as np

# Constants: Empty cell, Player X, Player O
E = 0
X = 1
O = 2

# List of possibles tris
tris = np.array([
    [0, 1, 2],  # rows
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],  # columns
    [2, 5, 8],
    [0, 4, 8],  # diagonals
    [2, 4, 6], ])


class Board(object):
    values = None
    who_played = O
    who_plays_next = X

    def __init__(self, parent=None):
        if parent is None:
            # create a new empty board
            self.values = np.zeros(9).reshape(3, 3)
            self.values.fill(E)
        else:
            # copy the cells from the parent and updates variables
            self.values = np.copy(parent.values)
            self.who_played = parent.who_plays_next
            self.who_plays_next = parent.who_played

    def has_won(self, player):
        flat_values = self.values.ravel()
        expected_schema = np.zeros((1, 3))
        expected_schema.fill(player)  # [X, X, X] or [O, O, O]

        # tris is a list of possibles tris combinations
        # flat_values[tris] returns the values of the board in the possible tris positions
        # == expected_schema compares the values in the board with the expected_schema
        # .all(axis=1) returns a list of booleans: it is true if a combination inside the board is equal to the expected_schema
        # .any() if any combination is true, it means that there is a tris
        return (flat_values[tris] == expected_schema).all(axis=1).any()

    def score(self, player):
        if self.has_won(player):
            return 10-self.depth()
        elif self.has_won(O if player == X else X):  # if has lost
            return self.depth() - 10
        else:
            return 0

    def depth(self):
        # count not empty cells
        return (self.values != E).sum()

    def is_over(self):
        # if there is an Empty spot, the game is not over
        return not (self.values[:, :] == E).any()

    def get_best_board(self, player):
        """
        Compute minimax algorithm to get the best board with its score (score,board) for the given player
        :return: (score, board)
        """
        current_score = self.score(player)

        if current_score != 0:
            return (current_score, self)
        elif self.is_over():
            return (0, self)

        next_score_board = []
        for x in xrange(3):
            for y in xrange(3):
                if self.values[x, y] == E:
                    next_board = self.play(x, y)
                    best_next_score = next_board.get_best_board(player)[0]
                    next_score_board.append((best_next_score, next_board))

        if self.who_plays_next == player:
            return max(next_score_board, key=lambda k: k[0])
        else:
            return min(next_score_board, key=lambda k: k[0])

    def play(self, x, y):
        """
        Make a move at the given position
        :return: the new board
        """
        next_board = Board(self)
        next_board.values[x, y] = self.who_plays_next
        return next_board

    def to_string(self):
        """
        :return: a printable string of the board
        """
        switcher = {
            E: ' ',
            X: 'X',
            O: 'O'
        }

        text = '\n'
        text += '-' * 3
        text += '\n'
        for y in range(3):
            for x in range(3):
                text += switcher.get(self.values[x, y])
            text += '\n'
        text += '-' * 3
        text += '\n'

        return text
