from tictactoe import Board
from tictactoe import X, E, O


def computer_vs_user():
    computer = X
    user = O
    board = (0, Board())
    print(board[1].to_string())

    while True:
        # Computer plays
        board = board[1].get_best_board(computer)
        print(board[1].to_string())

        if (board[1].has_won(computer)):
            print('Computer has won')
            break

        # User plays
        x = int(input('x: '))
        y = int(input('y: '))
        board = (0, board[1].play(x, y))
        print(board[1].to_string())
        if (board[1].has_won(user)):
            print('You won. Really? It is not possible!')
            break


def computer_vs_computer():
    computer1 = X
    computer2 = O
    board = (0, Board())
    print(board[1].to_string())

    while True:
        # Computer1 plays
        board = board[1].get_best_board(computer1)
        print(board[1].to_string())

        if (board[1].has_won(computer1)):
            print('Computer1 has won')
            break
        elif (board[1].is_over()):
            print('Nobody won')
            break

        # Computer2 plays
        board = board[1].get_best_board(computer2)
        print(board[1].to_string())

        if (board[1].has_won(computer2)):
            print('Computer2 has won')
            break
        elif (board[1].is_over()):
            print('Nobody won')
            break


if __name__ == "__main__":
    computer_vs_computer()
